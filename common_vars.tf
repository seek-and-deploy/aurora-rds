######################################################################
# AWS Variables
###
variable "aws_region"         { default = "us-east-1" }
variable "availability_zones" { type = list        }

######################################################################
# Common Veracode config settings
###
variable "env_name"           {                 }
variable "tags"               { type	= map }
variable "use_elb_asg"        { default = true  }


