variable "aws_profile"    { }
variable "account_id"	  { }
variable "route53_tld"	  { }
variable "public_zone_id" { }
variable "tmpdir"	  { }
variable "tf_state_bucket"{ }
variable "project_config" { }
variable "project"	  { }
variable "tf_state"	  { }

locals {
  env_name = lookup(var.environment, "name")
  env_vers = lookup(var.environment, "version")

  version = local.env_vers
  pub_dns_name = "${var.tags["system_type"]}-public-${local.version}"
  pri_dns_name = "${var.tags["system_type"]}-private-${local.version}"
}

######################################################################
# RDS Vars
##
variable "rds" {
  type=map
  default = {
    cluster_instance_count = 3,
    instance_class         = "db.r3.large" 
    db_name                = ""
    db_user                = ""
    db_password            = ""
  }
}

###
# Needed for the terraform-deployer
###
variable "tf_root"	    {}
variable "tfvars"	    {}
variable "terraform"	    {}
variable "env_folder"	    {}
variable "environment"      { type	= map }
