# -*- terrarform -*-

data "terraform_remote_state" "core" {
  backend = "s3"
  config  = {
    region = var.aws_region
    bucket = var.tf_state_bucket
    key    = "${local.env_name}.tfstate"
  }
}

module "rds" {
  source             = "./rds"
  availability_zones = var.availability_zones
  tags               = var.tags
  env_name           = local.env_name
  vpc_id             = data.terraform_remote_state.core.outputs.vpc_id
  allowed_cidr_blocks= data.terraform_remote_state.core.outputs.private_subnet_cidrs
  sg_ids             = data.terraform_remote_state.core.outputs.sg_ids
  pub_subnet_ids     = data.terraform_remote_state.core.outputs.pub_subnet_ids
  pri_subnet_ids     = data.terraform_remote_state.core.outputs.pri_subnet_ids
  pub_dns_domain_id  = data.terraform_remote_state.core.outputs.public_dns_domain_id
  pub_dns_name       = local.pub_dns_name 
  pub_dns_domain     = data.terraform_remote_state.core.outputs.public_dns_domain_name 
  pri_dns_domain_id  = data.terraform_remote_state.core.outputs.private_dns_domain_id
  pri_dns_name       = local.pri_dns_name 
  pri_dns_domain     = data.terraform_remote_state.core.outputs.private_dns_domain_name
  instance_role_id   = data.terraform_remote_state.core.outputs.reliance_ec2_client_role_id
  # RDS vars
  rds_map            = var.rds
}


output "rds-endpoint" {
  value = module.rds.rds-endpoint
}

