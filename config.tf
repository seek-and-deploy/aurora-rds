# -*- terrarform -*-
provider "aws" {
  region                = "us-east-1"
  profile               = var.aws_profile
}

# Define remote state backend as S3
terraform {
  # Store tfstate in S3 bucket
  backend "s3" {}
}
