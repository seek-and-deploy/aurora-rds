# -*- terraform -*-
######################################################################
# Add CNAME for DB to make it easy to access
##
resource "aws_route53_record" "rds" {
  zone_id = var.pri_dns_domain_id
  name = "${var.tags["system_type"]}-${var.env_name}-rds.${var.pri_dns_domain}"
  type = "CNAME"
  ttl = "300"
  records = [ aws_rds_cluster.rds.endpoint ]
}

output "rds-cluster-cname" {
  value = aws_route53_record.rds.fqdn
}

