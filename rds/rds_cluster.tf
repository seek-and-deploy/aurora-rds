resource "aws_rds_cluster" "rds" {
  depends_on              = [ aws_security_group.rds,
                              aws_db_subnet_group.rds ]
  engine                  = var.rds_map["db_type"]
  cluster_identifier      = "${var.tags["system_type"]}-${var.tags["owner"]}-${var.env_name}"
  availability_zones      = slice( var.availability_zones, 0, 2)
  database_name           = var.rds_map["db_name"]
  master_username         = var.rds_map["db_user"]
  master_password         = var.rds_map["db_password"]
  vpc_security_group_ids  = split(",", local.sg_ids)

  db_subnet_group_name    = aws_db_subnet_group.rds.id
  skip_final_snapshot     = true
  final_snapshot_identifier = "${var.tags["Name"]}-snapshot"
}

output "rds-endpoint" {
  value = aws_rds_cluster.rds.endpoint
}

# output "rds-cluster-status" {
#   value = aws_rds_cluster.rds.status
# }

######################################################################
# set up a null resource to force the RDS cluster to be up before
# trying to build anything else.
##
resource "null_resource" "rds-cluster" {
  depends_on = [ aws_rds_cluster.rds ]

  provisioner "local-exec" {
    command = "sleep 1; echo 'Waiting for rds-cluster'"
  }
}


resource "aws_rds_cluster_instance" "rds_instances" {
  count              = var.rds_map["cluster_instance_count"]
  depends_on         = [ aws_rds_cluster.rds ]
  identifier         = "${var.tags["system_type"]}-${var.tags["owner"]}-${var.env_name}-${count.index}"
  cluster_identifier = aws_rds_cluster.rds.id
  engine             = var.rds_map["db_type"]
  instance_class     = var.rds_map["instance_class"]
  db_subnet_group_name = aws_db_subnet_group.rds.id
  tags = merge(var.tags,
               map("Name", "${var.tags["system_type"]}-${var.tags["owner"]}-${var.env_name}-rds-${count.index}"),
               map("env",  "${ element( split("-", var.env_name),0) }"))
}

######################################################################
# set up a null resource to force the RDS cluster to be up before
# trying to build anything else.
##
resource "null_resource" "rds-instances" {
  depends_on = [ aws_rds_cluster.rds,
                 aws_rds_cluster_instance.rds_instances ]

  provisioner "local-exec" {
    command = "sleep 1; echo 'Waiting for rds-instances'"
  }
}


