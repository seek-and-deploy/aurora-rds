# -*- terraform -*-
######################################################################
# Passed in variables.
###
variable "allowed_cidr_blocks"   { }
variable "instance_role_id"      { }
variable "pri_dns_domain"        { }
variable "pri_dns_name"          { }
variable "pri_subnet_ids"        { }
variable "pri_dns_domain_id"     { }
variable "pub_dns_domain"        { }
variable "pub_dns_name"          { }
variable "pub_subnet_ids"        { }
variable "pub_dns_domain_id"     { }
variable "sg_ids"                { }
variable "vpc_id"                { }

######################################################################
# RDS Vars
##
variable "rds_map"               { type = map }

######################################################################
# Local vars
##
locals {
  allowed_cidr_blocks = split(",", var.allowed_cidr_blocks)
  pub_fqdn = "${var.pub_dns_name}.${var.pub_dns_domain}"
  pri_fqdn = "${var.pri_dns_name}.${var.pri_dns_domain}"
  sg_ids   = join(",", [ var.sg_ids, aws_security_group.rds.id] )
}


