######################################################################
# Create the required AWS Subnet Group from existing VPC Subnet IDs
##
resource "aws_db_subnet_group" "rds" {
  name        = "${var.tags["system_type"]}-${var.tags["owner"]}-${var.env_name}"
  description = "${var.tags["owner"]} ${var.tags["system_type"]} RDS for ${var.env_name}"
  subnet_ids  = split(",", var.pri_subnet_ids)
  tags        = merge(var.tags,
                      map("Name", "${var.tags["system_type"]}-${var.tags["owner"]}-${var.env_name}"),
                      map("env",  "${var.env_name}"))
}

output "rds_subnet_group_id" {
  value = aws_db_subnet_group.rds.id
}
output "rds_subnet_group_arn" {
  value = aws_db_subnet_group.rds.arn
}
