
######################################################################
# Create the policies from the rendered templates and attach them to
# the same role.
##
resource "aws_iam_role_policy" "rds_access" {
    name   = "${var.env_name}-${var.tags["owner"]}_rds_role_policy"
    role   = var.instance_role_id
  policy = templatefile("${path.module}/templates/iam_rds_role_policy.tpl", {})
}

output "rds_access_role_policy_id" {
  value = aws_iam_role_policy.rds_access.id
}
output "rds_access_role_policy_name" {
    value = aws_iam_role_policy.rds_access.name
}
output "rds_access_role_policy_policy" {
    value = aws_iam_role_policy.rds_access.policy
}
output "rds_access_role_policy_role" {
    value = aws_iam_role_policy.rds_access.role
}
