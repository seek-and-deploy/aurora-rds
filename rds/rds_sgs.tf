# -*- terraform -*-
######################################################################
# Allow MySQL access inbound from allowed CIDR blocks
resource "aws_security_group" "rds" {
  name        = "${var.tags["system_type"]}_${var.env_name}_rds"
  description = "Allow MT traffic inbound"
  vpc_id      = var.vpc_id
  tags = merge(var.tags, 
               map("Name", "${var.tags["system_type"]} ${var.env_name} Allow MT traffic SG inbound"),
               map("env",  "${var.env_name}"))

  ######################################################################
  # Inbound Traffic
  ##   
  # Allow ALL traffic from this SG
  ingress {
    from_port = 0
    to_port   = 0
    protocol  = "-1"
    self      = true
  }

  # MySQL/Aurora
  ingress {
    from_port   = 3606
    to_port     = 3606
    protocol    = "tcp"
    cidr_blocks = split(",", var.allowed_cidr_blocks)
  }  

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
